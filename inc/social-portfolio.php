<?php
/**
 * artist_showcase Theme Customizer.
 *
 * @package artist_showcase
 */

function artist_showcase_sp_customize( $wp_customize ) {
	$wp_customize->add_panel( 'social-portfolio', array( 'title' => __('Social Portfolio', 'artist_showcase'), 'priority' => '1' ) );


	$wp_customize->add_section( 'sp-facebook', array( 'title'    => __( 'Facebook', 'artist_showcase' ), 'panel' => 'social-portfolio', 'priority' => 1 ) );
	$wp_customize->add_section( 'sp-mixcloud', array( 'title'    => __( 'Mixcloud', 'artist_showcase' ), 'panel' => 'social-portfolio', 'priority' => 2 ) );
	$wp_customize->add_section( 'sp-soundcloud', array( 'title'    => __( 'Soundcloud', 'artist_showcase' ), 'panel' => 'social-portfolio', 'priority' => 3 ) );
	$wp_customize->add_section( 'sp-twitter', array( 'title'    => __( 'Twitter', 'artist_showcase' ), 'panel' => 'social-portfolio', 'priority' => 4 ) );
	$wp_customize->add_section( 'sp-youtube', array( 'title'    => __( 'Youtube', 'artist_showcase' ), 'panel' => 'social-portfolio', 'priority' => 5 ) );


	$wp_customize->add_setting( 'sp-facebook', array( 'default' => 'disabled' ) );
	$wp_customize->add_setting( 'sp-mixcloud', array( 'default' => 'disabled' ) );
	$wp_customize->add_setting( 'sp-mixcloud-username', array( 'default' => '' ) );
	$wp_customize->add_setting( 'sp-soundcloud', array( 'default' => 'disabled' ) );
	$wp_customize->add_setting( 'sp-soundcloud-username', array( 'default' => '' ) );
	$wp_customize->add_setting( 'sp-twitter', array( 'default' => 'disabled' ) );
	$wp_customize->add_setting( 'sp-twitter-username', array( 'default' => '' ) );
	$wp_customize->add_setting( 'sp-youtube', array( 'default' => 'disabled' ) );
	$wp_customize->add_setting( 'sp-youtube-username', array( 'default' => '' ) );


	$wp_customize->add_control( 'sp-facebook', array( 'label'    => __( 'Enable Facebook', 'artist_showcase' ), 'section'  => 'sp-facebook', 'settings' => 'sp-facebook', 'default' => 'false', 'type'     => 'select', 'choices'  => array(
			  'enabled'  => 'Enable',
			  'disabled' => 'Disable',
		  ),
	  )
  );
	$wp_customize->add_control( 'sp-mixcloud', array( 'label'    => __( 'Enable Mixcloud', 'artist_showcase' ), 'section'  => 'sp-mixcloud', 'settings' => 'sp-mixcloud', 'default' => 'false', 'type'     => 'select', 'choices'  => array(
			  'enabled'  => 'Enable',
			  'disabled' => 'Disable',
		  ),
	  )
  );
	$wp_customize->add_control( 'sp-mixcloud-user', array( 'label'    => __( 'Mixcloud Username', 'artist_showcase' ), 'section'  => 'sp-mixcloud', 'settings' => 'sp-mixcloud-username', 'default' => '', 'type'     => 'text',
	  )
  );
	$wp_customize->add_control( 'sp-soundcloud', array( 'label'    => __( 'Enable Soundcloud', 'artist_showcase' ), 'section'  => 'sp-soundcloud', 'settings' => 'sp-soundcloud', 'default' => 'false', 'type'     => 'select', 'choices'  => array(
			  'enabled'  => 'Enable',
			  'disabled' => 'Disable',
		  ),
	  )
  );
	$wp_customize->add_control( 'sp-soundcloud-user', array( 'label'    => __( 'Soundcloud Username', 'artist_showcase' ), 'section'  => 'sp-soundcloud', 'settings' => 'sp-soundcloud-username', 'default' => '', 'type'     => 'text',
	  )
  );
	$wp_customize->add_control( 'sp-twitter', array( 'label'    => __( 'Enable Twitter', 'artist_showcase' ), 'section'  => 'sp-twitter', 'settings' => 'sp-twitter', 'default' => 'false', 'type'     => 'select', 'choices'  => array(
			  'enabled'  => 'Enable',
			  'disabled' => 'Disable',
		  ),
	  )
  );
	$wp_customize->add_control( 'sp-soundcloud-user', array( 'label'    => __( 'Twitter Username', 'artist_showcase' ), 'section'  => 'sp-twitter', 'settings' => 'sp-twitter-username', 'default' => '', 'type'     => 'text',
	  )
  );
	$wp_customize->add_control( 'sp-youtube', array( 'label'    => __( 'Enable youtube', 'artist_showcase' ), 'section'  => 'sp-youtube', 'settings' => 'sp-youtube', 'default' => 'false', 'type'     => 'select', 'choices'  => array(
			  'enabled'  => 'Enable',
			  'disabled' => 'Disable',
		  ),
	  )
  );
	$wp_customize->add_control( 'sp-youtube-user', array( 'label'    => __( 'youtube Username', 'artist_showcase' ), 'section'  => 'sp-youtube', 'settings' => 'sp-youtube-username', 'default' => '', 'type'     => 'text',
	  )
  );

}
add_action( 'customize_register', 'artist_showcase_sp_customize' );

