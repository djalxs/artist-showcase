<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package artist_showcase
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
      <div class="row">
        <div class="large-12 columns">
          <h1 class="sp-title">Welcome to your social portfolio</h1>
<?php
$mods = get_theme_mods();
foreach ($mods as $key => $value) {
    if ( $value == 'enabled' ) {
      include get_template_directory() . '/social-portfolio-parts/' . $key . '.php';
    }
}

?> 
        </div>
      </div>
		</main>
	</div>

<?php get_footer(); ?>
