jQuery(document).foundation();

$(document).ready( function() {

  var $count = $("#primary-menu").children('.menu-item').length;
  var $linkwidth = 100/$count;

  var $windowWidth = $(window).width() / parseFloat($("body").css("font-size"));
  if ($windowWidth >= 40.0625 && $('.site-header').hasClass('unstuck') ) {
    $("#primary-menu > li.menu-item").css({'width': $linkwidth + '%'});
  }
});
$(window).resize( function() {

  var $count = $("#primary-menu").children('.menu-item').length;
  var $linkwidth = 100/$count;
  var $windowWidth = $(window).width() / parseFloat($("body").css("font-size"));

  if ( $windowWidth >= 40.0625 &&  !$('.site-header').hasClass('unstuck') ){  
    $(".site-header.unstuck #primary-menu > li.menu-item").css({'width': $linkwidth + '%'});
  } else {
    $(".site-header.unstuck #primary-menu > li.menu-item").css({'width': ''});
  }
});
$(document).ready(function() {
    
    /* Every time the window is scrolled ... */
    $(window).scroll( function(){
    
        /* Check the location of each desired element */
        $('article').each( function(i){
            
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height() / 2;
            
            /* If the object is completely visible in the window, fade it it */
            if( bottom_of_window > bottom_of_object ){
                
                $(this).fadeIn(1000);
                    
            }
            
        }); 
    
    });
    
});
